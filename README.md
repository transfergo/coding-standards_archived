# TransferGo PHP Coding Standards

This repository contains sets of code style rules that apply to TransferGo PHP codebase.

## Standard sets
Different project set ups require different coding standards. Currently, there is base **TransferGo PHP Coding Standard** set that
 applies to all the PHP projects. It is extended by more customised sets that apply to a subset of projects, e.g. 
 **TransferGo PHP 7.1 Coding Standard** that should be used in projects that run PHP 7.1.
 
## Usage
 Library can be included by adding
 
```
"transfergo/coding-standards": {
    "type": "git",
    "url": "git@bitbucket.org:transfergo/coding-standards.git"
}
```

to composer.json repositories and requiring standards library via composer:

```
composer require transfergo/coding-standards --dev
```

To run the code style checks before commit setup pre-commit git hook (make sure this runs in project root):

```
filesStr=$(git diff --cached --name-only --diff-filter=ACM | grep '.php$' | xargs);

echo "$filesStr"
IFS=' '
read -ra OUTPUT <<< "$filesStr"
for i in "${OUTPUT[@]}"; do
        ./vendor/bin/phpcs --standard=./vendor/transfergo/coding-standards/standards/ruleset_LIE.xml --extensions=php ../$OUTPUT
done

```
where --standard flag specifies the standard you want to apply, e.g.
```
./vendor/transfergo/coding-standards/standards/ruleset.xml
```
for base standards application, or
```
./vendor/transfergo/coding-standards/standards/ruleset_7_1.xml
```

for standards that apply to PHP 7.1 projects.

## Extending repository
Sniffs that apply to a limited group of projects should be placed in their own directory. Rule set that applies to particular 
subset of projects should be defined in ruleset.xml file in dedicated directory, e.g. standards/TGF/ruleset.xml. Then the
new set can be used by adding new ruleset XML file in the top level directory, e.g. standards/ruleset.xml.

Top level rule sets should indicate in their name what sort of projects they are built for. All top level rule sets are 
expected to include the base rule set with <rule ref="TGF" />.

Please provide documentation for the new sniffs.

PHP_CodeSniffer is very particular about the folder structure where sniffs can be found, please 
follow existing examples when extending the repositories. Don't forget to provide
the new rule set paths to PHP_CodeSniffer in projects where you wish to use them (see --config-set command above).

## What sniffs are available?
Unfortunately, pre installed sniffs don't come with full documentation. You can see partial list by running

```
vendor/bin/phpcs --generator > output.html
```

and viewing generated document in a browser.

## Where to find applicable rules and examples
We have dev team guidelines repository that contains information about all the standards applicable to the backend projects,
please see https://bitbucket.org/transfergo/devteam-guidelines/src/master/ for more information and examples.