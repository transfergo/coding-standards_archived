<?php

declare(strict_types=1);

namespace Standards\TGF\Sniffs\Commenting;

use PHP_CodeSniffer\Files\File;

/**
 * 'var' tag comment validator
 *
 * @package Standards\TGF
 */
class VarDocCommentSniff extends AbstractDocCommentSniff
{
    /**
     * Processes this test, when one of its tokens is encountered.
     *
     * @param File $phpcsFile The file being scanned.
     * @param int  $stackPtr  The position of the current token in the stack passed in $tokens.
     *
     * @return void
     */
    public function process(File $phpcsFile, $stackPtr): void
    {
        parent::process($phpcsFile, $stackPtr);
        $tokens = $phpcsFile->getTokens();
        $commentStart = $stackPtr;
        $commentEnd   = $tokens[$stackPtr]['comment_closer'];

        if (!$this->isVarDocCommentTag($tokens, $commentStart, $commentEnd)) {
            // Doc comment does not contain @var tag.
            return;
        }

        $token = $tokens[$commentStart + 1];
        if ($token['code'] != T_DOC_COMMENT_WHITESPACE || $token['content'] != ' ') {
            $phpcsFile->addError(
                'Expected single space',
                $commentStart + 1,
                'SingleSpaceBeforeVarTag'
            );

            return;
        }

        $token = $tokens[$commentStart + 2];
        if ($token['code'] != T_DOC_COMMENT_TAG || $token['content'] != '@var') {
            $phpcsFile->addError(
                'Expected var tag on same line',
                $commentStart + 2,
                'VarTagOnSameLine'
            );

            return;
        }

        $token = $tokens[$commentStart + 3];
        if ($token['code'] != T_DOC_COMMENT_WHITESPACE || $token['content'] != ' ') {
            $phpcsFile->addError(
                'Expected single space after var tag',
                $commentStart + 3,
                'SingleSpaceAfterVarTag'
            );

            return;
        }

        $token = $tokens[$commentStart + 4];
        $commentContent = $token['content'];
        if ($token['code'] != T_DOC_COMMENT_STRING) {
            $phpcsFile->addError(
                'Expected variable type',
                $commentStart + 4,
                'VarTypeOnSameLine'
            );

            return;
        }

        $token = $tokens[$commentStart + 5];
        if ($token['code'] != T_DOC_COMMENT_CLOSE_TAG) {
            $phpcsFile->addError(
                'Expected comment close after variable type',
                $commentStart + 5,
                'SingleLineDocComment'
            );

            return;
        }

        [$className] = explode(' ', $commentContent);
        $pattern = '/^((\\\\){0,1}[a-zA-Z_][a-zA-Z0-9_]*)*(\\|{0,1}(\\\\){0,1}[a-zA-Z_][a-zA-Z0-9_]*)*$/';
        if (!preg_match($pattern, $className)) {
            $phpcsFile->addError(
                'Expected valid variable type: ' . $className,
                $commentStart + 4,
                'SingleLineDocComment'
            );

            return;
        }

        $singleEndSpacePattern = '/\w\s$/';
        if (!preg_match($singleEndSpacePattern, $commentContent)) {
            $phpcsFile->addError(
                'Expected single space before closing comment',
                $commentEnd - 1,
                'SingleLineDocComment'
            );

            return;
        }
    }

    /**
     * Checks whether comment contains var tag
     *
     * @param array $tokens       All tokens
     * @param int   $commentStart Comment start position
     * @param int   $commentEnd   Comment end position
     *
     * @return bool
     */
    private function isVarDocCommentTag(array $tokens, int $commentStart, int $commentEnd): bool
    {
        for ($i = $commentStart; $i <= $commentEnd; $i++) {
            if ($tokens[$i]['code'] == T_DOC_COMMENT_TAG && $tokens[$i]['content'] == '@var') {
                return true;
            }
        }

        return false;
    }
}
