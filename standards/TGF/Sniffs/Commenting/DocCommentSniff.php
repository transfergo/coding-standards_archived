<?php

declare(strict_types=1);

namespace Standards\TGF\Sniffs\Commenting;

use PHP_CodeSniffer\Files\File;

/**
 * Docblock validator
 *
 * @package Standards\TGF
 */
class DocCommentSniff extends AbstractDocCommentSniff
{
    const PARAM_TAG = '@param';

    /** @var array  A list of tokenizers this sniff supports */
    public $supportedTokenizers = ['PHP'];

    /**
     * Returns an array of tokens this test wants to listen for.
     *
     * @return array
     */
    public function register(): array
    {
        return [T_DOC_COMMENT_OPEN_TAG];
    }

    /**
     * Processes this test, when one of its tokens is encountered.
     *
     * @param File $phpcsFile The file being scanned.
     * @param int  $stackPtr  The position of the current token in the stack passed in $tokens.
     *
     * @return void
     */
    public function process(File $phpcsFile, $stackPtr): void
    {
        parent::process($phpcsFile, $stackPtr);
        $tokens = $phpcsFile->getTokens();
        $commentStart = $stackPtr;
        $commentEnd = $tokens[$stackPtr]['comment_closer'];

        $empty = [
            T_DOC_COMMENT_WHITESPACE,
            T_DOC_COMMENT_STAR,
        ];

        // Ignore /** @var ... */ comments, they should be validated with a dedicated sniff
        if ($tokens[$commentStart + 2]['content'] === '@var') {
            return;
        }

        $secondBlockLine = $phpcsFile->findNext($empty, ($stackPtr + 1), $commentEnd, true);
        if ($secondBlockLine === false) {
            // No content at all.
            $error = 'Doc comment is empty';
            $phpcsFile->addError($error, $stackPtr, 'Empty');
            return;
        }

        // The first line of the comment should just be the /** code.
        if ($tokens[$secondBlockLine]['line'] === $tokens[$stackPtr]['line']) {
            $error = 'The open comment tag must be the only content on the line';
            $phpcsFile->addFixableError($error, $stackPtr, 'ContentAfterOpen');
        }

        // The last line of the comment should just be the */ code.
        $prev = $phpcsFile->findPrevious($empty, ($commentEnd - 1), $stackPtr, true);
        if ($tokens[$prev]['line'] === $tokens[$commentEnd]['line']) {
            $error = 'The close comment tag must be the only content on the line';
            $phpcsFile->addFixableError($error, $commentEnd, 'ContentBeforeClose');
        }

        // Check if comment contains useful information
        if (!$this->hasMethodDescription($tokens, $secondBlockLine)
            && !$this->hasUsefulParamDescription($tokens, $secondBlockLine, $commentEnd)
            && !$this->hasExceptionDocumentation($tokens, $secondBlockLine, $commentEnd)
            && !$this->hasUsefulReturnTag($tokens, $secondBlockLine, $commentEnd)
        ) {
            $error = 'Doc comment does not contain any useful information';
            $phpcsFile->addError($error, $stackPtr, 'Empty');
            return;
        }

        // Check for additional blank lines at the end of the comment.
        if ($tokens[$prev]['line'] < ($tokens[$commentEnd]['line'] - 1)) {
            $error = 'Additional blank lines found at end of doc comment';
            $phpcsFile->addFixableError($error, $commentEnd, 'SpacingAfter');
        }

        // Check for a comment description.
        if ($tokens[$secondBlockLine]['code'] === T_DOC_COMMENT_STRING) {
            // No extra newline before short description.
            if ($tokens[$secondBlockLine]['line'] !== ($tokens[$stackPtr]['line'] + 1)) {
                $error = 'Doc comment short description must be on the first line';
                $phpcsFile->addFixableError($error, $secondBlockLine, 'SpacingBeforeShort');
            }

            // Account for the fact that a short description might cover
            // multiple lines.
            $blockDescriptionContent = $tokens[$secondBlockLine]['content'];
            $blockDescriptionContentEnd = $secondBlockLine;
            for ($i = ($secondBlockLine + 1); $i < $commentEnd; $i++) {
                if ($tokens[$i]['code'] === T_DOC_COMMENT_STRING) {
                    if ($tokens[$i]['line'] === ($tokens[$blockDescriptionContentEnd]['line'] + 1)) {
                        $blockDescriptionContent .= $tokens[$i]['content'];
                        $blockDescriptionContentEnd = $i;
                    } else {
                        break;
                    }
                }
            }

            if (preg_match('/^\p{Ll}/u', $blockDescriptionContent) === 1) {
                $error = 'Doc comment short description must start with a capital letter';
                $phpcsFile->addError($error, $secondBlockLine, 'ShortNotCapital');
            }

            $additionalDescription = $phpcsFile->findNext($empty, ($blockDescriptionContentEnd + 1), ($commentEnd - 1), true);
            if ($additionalDescription !== false && $tokens[$additionalDescription]['code'] === T_DOC_COMMENT_STRING) {
                if ($tokens[$additionalDescription]['line'] !== ($tokens[$blockDescriptionContentEnd]['line'] + 2)) {
                    $error = 'There must be exactly one blank line between descriptions in a doc comment';
                    $phpcsFile->addFixableError($error, $additionalDescription, 'SpacingBetween');
                }

                if (preg_match('/^\p{Ll}/u', $tokens[$additionalDescription]['content']) === 1) {
                    $error = 'Doc comment long description must start with a capital letter';
                    $phpcsFile->addError($error, $additionalDescription, 'LongNotCapital');
                }
            }
        }

        if (empty($tokens[$commentStart]['comment_tags'])) {
            // No tags in the comment.
            return;
        }

        $firstTag = $tokens[$commentStart]['comment_tags'][0];
        $prev = $phpcsFile->findPrevious($empty, ($firstTag - 1), $stackPtr, true);
        if ($tokens[$firstTag]['line'] !== ($tokens[$prev]['line'] + 2)
            && $tokens[$prev]['code'] !== T_DOC_COMMENT_OPEN_TAG
        ) {
            $error = 'There must be exactly one blank line before the tags in a doc comment';
            $phpcsFile->addFixableError($error, $firstTag, 'SpacingBeforeTags');
        }

        // Break out the tags into groups and check alignment within each.
        // A tag group is one where there are no blank lines between tags.
        // The param tag group is special as it requires all @param tags to be inside.
        $tagGroups = [];
        $groupid = 0;
        $paramGroupid = null;
        foreach ($tokens[$commentStart]['comment_tags'] as $pos => $tag) {
            if ($pos > 0) {
                $prev = $phpcsFile->findPrevious(
                    T_DOC_COMMENT_STRING,
                    ($tag - 1),
                    $tokens[$commentStart]['comment_tags'][($pos - 1)]
                );

                if ($prev === false) {
                    $prev = $tokens[$commentStart]['comment_tags'][($pos - 1)];
                }

                if ($tokens[$prev]['line'] !== ($tokens[$tag]['line'] - 1)) {
                    $groupid++;
                }
            }

            if ($tokens[$tag]['content'] === self::PARAM_TAG) {
                if ($paramGroupid !== null
                    && $paramGroupid !== $groupid
                ) {
                    $error = 'Parameter tags must be grouped together in a doc comment';
                    $phpcsFile->addError($error, $tag, 'ParamGroup');
                }

                if ($paramGroupid === null) {
                    $paramGroupid = $groupid;
                }
            }

            $tagGroups[$groupid][] = $tag;
        }

        foreach ($tagGroups as $groupid => $group) {
            $maxLength = 0;
            $paddings = [];
            foreach ($group as $pos => $tag) {
                if ($paramGroupid === $groupid
                    && $tokens[$tag]['content'] !== self::PARAM_TAG
                ) {
                    $error = 'Tag %s cannot be grouped with parameter tags in a doc comment';
                    $data = [$tokens[$tag]['content']];
                    $phpcsFile->addError($error, $tag, 'NonParamGroup', $data);
                }

                $tagLength = $tokens[$tag]['length'];
                if ($tagLength > $maxLength) {
                    $maxLength = $tagLength;
                }

                // Check for a value. No value means no padding needed.
                $string = $phpcsFile->findNext(T_DOC_COMMENT_STRING, $tag, $commentEnd);
                if ($string !== false && $tokens[$string]['line'] === $tokens[$tag]['line']) {
                    $paddings[$tag] = $tokens[($tag + 1)]['length'];
                }
            }

            // Check that there was single blank line after the tag block
            // but account for a multi-line tag comments.
            $lastTag = $group[$pos];
            $next = $phpcsFile->findNext(T_DOC_COMMENT_TAG, ($lastTag + 3), $commentEnd);
            if ($next !== false) {
                $prev = $phpcsFile->findPrevious([T_DOC_COMMENT_TAG, T_DOC_COMMENT_STRING], ($next - 1), $commentStart);
                if ($tokens[$next]['line'] !== ($tokens[$prev]['line'] + 2)) {
                    $error = 'There must be a single blank line after a tag group';
                    $phpcsFile->addFixableError($error, $lastTag, 'SpacingAfterTagGroup');
                }
            }

            // Now check paddings.
            foreach ($paddings as $tag => $padding) {
                $required = ($maxLength - $tokens[$tag]['length'] + 1);

                if ($padding !== $required) {
                    $error = 'Tag value for %s tag indented incorrectly; expected %s spaces but found %s';
                    $data = [
                        $tokens[$tag]['content'],
                        $required,
                        $padding,
                    ];

                    $phpcsFile->addFixableError($error, ($tag + 1), 'TagValueIndent', $data);
                }
            }
        }

        // If there is a param group, it needs to be first.
        if ($paramGroupid !== null && $paramGroupid !== 0) {
            $error = 'Parameter tags must be defined first in a doc comment';
            $phpcsFile->addError($error, $tagGroups[$paramGroupid][0], 'ParamNotFirst');
        }

        $foundTags = [];
        foreach ($tokens[$stackPtr]['comment_tags'] as $pos => $tag) {
            $tagName = $tokens[$tag]['content'];
            if (isset($foundTags[$tagName]) === true) {
                $lastTag = $tokens[$stackPtr]['comment_tags'][($pos - 1)];
                if ($tokens[$lastTag]['content'] !== $tagName) {
                    $error = 'Tags must be grouped together in a doc comment';
                    $phpcsFile->addError($error, $tag, 'TagsNotGrouped');
                }

                continue;
            }

            $foundTags[$tagName] = true;
        }
    }

    /**
     * Checks whether given comment contains method description
     *
     * @param array $tokens            All comment tokens
     * @param int   $commentStartIndex Index that indicates the start of currently parsed comment
     *
     * @return bool
     */
    private function hasMethodDescription(array $tokens, int $commentStartIndex): bool
    {
        return $tokens[$commentStartIndex]['code'] === T_DOC_COMMENT_STRING;
    }

    /**
     * Checks whether given comment contains param descriptions that include information not available in
     * method signature
     *
     * @param array $tokens            All comment tokens
     * @param int   $commentStartIndex Index that indicates the start of currently parsed comment
     * @param int   $commentEndIndex   Index that indicates the end of currently parsed comment
     *
     * @return bool
     */
    private function hasUsefulParamDescription(array $tokens, int $commentStartIndex, int $commentEndIndex): bool
    {
        $currentIndex = $commentStartIndex;
        while ($currentIndex <= $commentEndIndex) {
            $token = $tokens[$currentIndex];
            if ($token['code'] === T_DOC_COMMENT_TAG && $token['content'] === self::PARAM_TAG) {
                $paramDescription = $tokens[$currentIndex + 2];
                $comment = $paramDescription['content'];
                if (strpos($comment, '[]') !== false) {
                    return true;
                }

                if (count(array_filter(explode(' ', $comment))) > 2) {
                    return true;
                }
            }

            $currentIndex++;
        }

        return false;
    }

    /**
     * Checks whether given comment contains thrown exception information
     *
     * @param array $tokens            All comment tokens
     * @param int   $commentStartIndex Index that indicates the start of currently parsed comment
     * @param int   $commentEndIndex   Index that indicates the end of currently parsed comment
     *
     * @return bool
     */
    private function hasExceptionDocumentation(array $tokens, int $commentStartIndex, int $commentEndIndex): bool
    {
        $currentIndex = $commentStartIndex;
        while ($currentIndex <= $commentEndIndex) {
            $token = $tokens[$currentIndex];
            if ($token['code'] === T_DOC_COMMENT_TAG && $token['content'] === '@throws') {
                return !empty($tokens[$currentIndex + 2]['content']);
            }

            $currentIndex++;
        }

        return false;
    }

    /**
     * Checks whether given comment contains contains any return information that cannot be provided in method signature
     *
     * @param array $tokens            All comment tokens
     * @param int   $commentStartIndex Index that indicates the start of currently parsed comment
     * @param int   $commentEndIndex   Index that indicates the end of currently parsed comment
     *
     * @return bool
     */
    private function hasUsefulReturnTag(array $tokens, int $commentStartIndex, int $commentEndIndex): bool
    {
        $currentIndex = $commentStartIndex;
        while ($currentIndex <= $commentEndIndex) {
            $token = $tokens[$currentIndex];
            if ($token['code'] === T_DOC_COMMENT_TAG && $token['content'] === '@return') {
                $descriptionToken = $tokens[$currentIndex + 2];
                if (!$descriptionToken['code'] === T_DOC_COMMENT_STRING) {
                    return false;
                }
                $descriptionTokenParts = explode(' ', $descriptionToken['content']);
                $types = $descriptionTokenParts[0] ?? '';
                $description = $descriptionTokenParts[1] ?? '';

                $numHintedTypes = count(array_filter(explode('|', $types)));
                if (strpos($types, 'null') !== false) {
                    --$numHintedTypes;
                }

                // If return comment has more than 1 type hint (count excludes 'null') or describes array elements
                // or has a description, it's a valid comment
                return $numHintedTypes > 1 || strpos($types, '[]') || !empty($description);
            }

            $currentIndex++;
        }

        return false;
    }
}
