<?php

declare(strict_types=1);

namespace Standards\TGF\Sniffs\Commenting;

use PHP_CodeSniffer\Sniffs\Sniff;
use PHP_CodeSniffer\Files\File;

/**
 * Functionality common to PHP comment validators
 *
 * @package Standards\TGF
 */
abstract class AbstractDocCommentSniff implements Sniff
{
    /** @var array  A list of tokenizers this sniff supports */
    public $supportedTokenizers = ['PHP'];

    /**
     * Returns an array of tokens inheriting test will listen for.
     *
     * @return array
     */
    public function register(): array
    {
        return [T_DOC_COMMENT_OPEN_TAG];
    }

    /**
     * Processes this test, when one of its tokens is encountered.
     *
     * @param File $phpcsFile The file being scanned.
     * @param int  $stackPtr  The position of the current token in the stack passed in $tokens.
     *
     * @return void
     */
    public function process(File $phpcsFile, $stackPtr): void
    {
        $tokens = $phpcsFile->getTokens();

        if (isset($tokens[$stackPtr]['comment_closer']) === false
            || ($tokens[$tokens[$stackPtr]['comment_closer']]['content'] === ''
                && $tokens[$stackPtr]['comment_closer'] === ($phpcsFile->numTokens - 1))
        ) {
            // Don't process an unfinished comment during live coding.
            return;
        }
    }
}
